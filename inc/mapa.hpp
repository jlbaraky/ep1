#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>

using namespace std;


class Mapa
{

	private:
		
		int tamanho;
		string *mapa;


	public:

		Mapa();
		Mapa(int tamanho, string* mapa);
		~Mapa();

		int get_tamanho();
		void set_tamanho(int tamannho);

		void gera_mapa();

};


#endif
