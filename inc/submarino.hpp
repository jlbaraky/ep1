#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "embarcacao.hpp"
#include <string>

using namespace std;


class Submarino : public Embarcacao
{

	private:
		int sobrevida;

	public:

		Submarino();
		Submarino(int eixo_x, int eixo_y, string orientacao);
		~Submarino();
	
		int get_sobrevida();
		void set_sobrevida(int sobrevida);

		int danificar(int eixo_x, int eixo_y);

		int rastrear(int eixo_x, int eixo_y);
};

#endif
