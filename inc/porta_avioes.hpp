#ifndef PORTA_AVIOES_HPP 
#define PORTA_AVIOES_HPP

#include "embarcacao.hpp"
#include <string>

using namespace std;


class Porta_avioes : public Embarcacao
{

	private:

		


	public:

		Porta_avioes();
		Porta_avioes(int eixo_x, int eixo_y, string orientacao);
		~Porta_avioes();


		int danificar();

		int rastrear(int eixo_x, int eixo_y);
};

#endif

