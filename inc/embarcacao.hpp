#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP
#include <string>

using namespace std;


class Embarcacao
{

	private:
		
		int tamanho;
		int eixo_x;
		int eixo_y;
		int vida;
		string orientacao;
	

	public:

		Embarcacao();
		~Embarcacao();

		int get_tamanho();
		void set_tamanho(int tamanho);

		int get_eixo_x();
		void set_eixo_x(int eixo_x);
	
		int get_eixo_y();
		void set_eixo_y(int eixo_y);

		int get_vida();
		void set_vida(int vida);

		string get_orientacao();
		void set_orientacao(string orientacao);
	
		virtual int danificar();

		virtual int rastrear(int eixo_x, int eixo_y);

};


#endif
