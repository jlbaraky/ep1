#ifndef CANOA_HPP
#define CANOA_HPP

#include "embarcacao.hpp"
#include <string>

using namespace std;


class Canoa : public Embarcacao
{

	private:

		


	public:
		
		Canoa();
		Canoa(int eixo_x, int eixo_y);
		~Canoa();

		int danificar();

		int rastrear(int eixo_x, int eixo_y);
};

#endif
