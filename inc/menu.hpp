#ifndef MENU_HPP
#define MENU_HPP


using namespace std;


class Menu
{

	private:


	public:

		Menu();
		~Menu();

		void inicio();

		void jogar();

		void instrucoes();

		void ranking();

		void limpa_tela();

		void espacamento(int num);

		void limpa_ranking();

};

#endif
