#ifndef JOGADOR_HPP
#define JOGADOR_HPP
#include "embarcacao.hpp"
#include "mapa.hpp"
#include <string>

using namespace std;


class Jogador
{

	private:

		string nome;
		int num_embarcacoes;
		int pontuacao;
		Embarcacao **embarcacoes;
		Mapa *mapa;


	public:
		
		Jogador();
		Jogador(string nome);
		~Jogador();

		string get_nome();
		void set_nome(string nome);

		int get_num_embarcacoes();
		void set_num_embarcacoes(int num_embarcacoes);

		int get_pontuacao();
		void set_pontuacao(int pontuacao);

		Embarcacao** get_embarcacoes();
		void set_embarcacoes(Embarcacao **embarcacoes);

		Mapa* get_mapa();
		void set_mapa(int tamanho, string* mapa);

		void tabuleiro();
		
		int atacar(Jogador *jogador, int eixo_x, int eixo_y);

		void subtrair_embarcacoes();

};


#endif
