#ifndef JOGO_HPP
#define JOGO_HPP

#include <string>
#include "jogador.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "porta_avioes.hpp"

using namespace std;

class Jogo
{

	private:
	
		Jogador* jogador1;
		Jogador* jogador2;

	public:

		Jogo();
		Jogo(Jogador* jogador1, Jogador* jogador2);
		~Jogo();

		void jogadores();

		Embarcacao* retorna_embarcacao(int eixo_x, int eixo_y, string tipo, string orientacao);

		void cria_embarcacao(Embarcacao** embarcacoes1, Embarcacao** embarcacoes2);

		void le_coordenadas(int *eixo_x, int *eixo_y);

		void turno(Jogador *jogador1, Jogador *jogador2, string *mapa_jogador);

		Jogador* vencedor();

		void atualiza_ranking(Jogador* jogador);

		void limpa_tela();



};


#endif
