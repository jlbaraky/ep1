## Descrição do Jogo

Rinha de Barco (Batalha Naval) é um jogo de tabuleiro no qual 2 jogadores guerreiam entre sí. Cada jogador possui 12 embarcações num tabuleiro 13x13, sendo elas:

* 6 canos;
* 4 submarinos;
* 2 porta aviões.

Vence aquele derrubar todas as embarcações inimigas, antes que ele derrube as suas.

## Embarcações

Este jogo, diferente do tradicional Battleship, possui embarcações com características diferentes: habilidades especiais.


* Canoa: simpes e frágil, ocupa apenas 1 casa e não possui habilidade;

* Submarino: compacto e resistente, ocupa 2 casas e é necessário atingir 2x cada casa para destrui-lo;

* Porta aviões: é o que mais temos de tecnologia no momento, ocupa 4 casas e tem a chance de desviar o míssil quando atirado em sua direção.


## Instruções

Cada jogador tem direito a 1 ataque por turno, sendo esse feito a partir da escolha de uma determinada coordenada (Ex: 6 D).
É verificado se há a presença de alguma embarcações inimiga resultando em diferentes feedbacks dependendo do alvo:

* [ - ] Indica que o ataque foi falho e o míssil caiu na água;
* [ o ] O ataque foi bem sucedido mas não abateu por completo - Ocorre quando o Porta aviões devia o míssil ou atinge pela primeira vez um Submarino;
* [ x ] A estrutura da embarcação no local foi destruída;


## Execução do Jogo

Para compilar o jogo:
```
$ make
```

Para executar o jogo:
```
$ make run
```

