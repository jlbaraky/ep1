#include "jogador.hpp"
#include <iostream>


Jogador::Jogador()
{
	
	nome = "";
	num_embarcacoes = 12;
	embarcacoes = NULL;
	pontuacao = 0;
	mapa = NULL;
}

Jogador::Jogador(string nome)
{
	this->nome = nome;
	this->num_embarcacoes = 12;
}

Jogador::~Jogador(){}

string Jogador::get_nome()
{
	return nome;
}

void Jogador::set_nome(string nome)
{
	this->nome = nome;
}

int Jogador::get_num_embarcacoes()
{
	return num_embarcacoes;
}

void Jogador::set_num_embarcacoes(int num_embarcacoes)
{
	this->num_embarcacoes = num_embarcacoes;
}

int Jogador::get_pontuacao()
{
	return pontuacao;
}

void Jogador::set_pontuacao(int pontuacao)
{
	this->pontuacao = pontuacao;
}

Embarcacao** Jogador::get_embarcacoes()
{
	return embarcacoes;
}

void Jogador::set_embarcacoes(Embarcacao **embarcacoes)
{
	this->embarcacoes = embarcacoes;
}

Mapa* Jogador::get_mapa()
{
	return mapa;
}

void Jogador::set_mapa(int tamanho, string* mapa)
{
	this->mapa = new Mapa(tamanho, mapa);
}


void Jogador::tabuleiro()
{
	mapa->gera_mapa();
}

int Jogador::atacar(Jogador *jogador, int eixo_x, int eixo_y)
{
	Embarcacao **embarcacoes_inimigas;

	embarcacoes_inimigas = jogador->get_embarcacoes();
	
	for(int i=0; i<12; i++){
		
		int acao = embarcacoes_inimigas[i]->rastrear(eixo_x, eixo_y);

		if(acao == 1 || acao == 2 || acao == 3)
	   		return acao;	
	}

	return 0;
}

void Jogador::subtrair_embarcacoes()
{

	set_num_embarcacoes(get_num_embarcacoes()-1);
}
