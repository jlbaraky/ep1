#include "porta_avioes.hpp"
#include <iostream>

#include <stdlib.h>


Porta_avioes::Porta_avioes()
{

	set_tamanho(4);
	set_eixo_x(0);
	set_eixo_y(0);
	set_orientacao("baixo");
	set_vida(4);
}

Porta_avioes::Porta_avioes(int eixo_x, int eixo_y, string orientacao)
{

	set_eixo_x(eixo_x);
	set_eixo_y(eixo_y);
	set_orientacao(orientacao);
	set_vida(4);
	set_tamanho(4);

	while(get_orientacao() == "cima" && eixo_y < 3)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}

	while(get_orientacao() == "baixo" && eixo_y > 9)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}

	while(get_orientacao() == "direita" && eixo_x > 9)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}

	while(get_orientacao() == "esquerda" && eixo_x < 3)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}	
}

Porta_avioes::~Porta_avioes(){}

int Porta_avioes::danificar()
{
	
	int chance;

	chance = rand() % 2 + 1;

	if(chance == 1){
		cout << endl << endl << endl << " >> Um porta aviões abateu o seu míssil, tente novamente" << endl;

		return 3;
	}

	else if(get_vida() != 1)
	{
		set_vida(get_vida()-1);
		cout << endl << endl << endl << " >> Você destruiu parte de um porta aviões!" << endl;
		return 2;
	}

	else if(get_vida() == 1)
	{
		set_vida(0);
		cout << endl << endl << endl << " >> Você destruiu um porta aviões!" << endl;
		return 1;
	}

	else
		return 0;	
}

int Porta_avioes::rastrear(int eixo_x, int eixo_y)
{


	if(get_eixo_x() == eixo_x && get_eixo_y() == eixo_y)
	{
		return danificar();
	}

	else if(get_orientacao() == "direita" && get_eixo_y() == eixo_y && ( eixo_x == get_eixo_x()+1 || eixo_x == get_eixo_x()+2 || eixo_x == get_eixo_x()+3))
	{
		return danificar();
	}

	else if(get_orientacao() == "esquerda" && get_eixo_y() == eixo_y && ( eixo_x == get_eixo_x()-1 || eixo_x ==  get_eixo_x()-2 || eixo_x == get_eixo_x()-3))
	{
		return danificar();
	}

	else if(get_orientacao() == "cima" &&  get_eixo_x() == eixo_x && ( eixo_y == get_eixo_y()-1 || eixo_y == get_eixo_y()-2 || eixo_y == get_eixo_y()-3))
	{
		return danificar();
	}

	else if(get_orientacao() == "baixo" && get_eixo_x() == eixo_x && ( eixo_y == get_eixo_y()+1 || eixo_y == get_eixo_y()+2 || eixo_y == get_eixo_y()+3))
	{
		return danificar();
	}

	else
		return 0;

}
