#include "canoa.hpp"
#include <iostream>


Canoa::Canoa()
{

	set_tamanho(1);
	set_eixo_x(0);
	set_eixo_y(0);
	set_orientacao("nenhuma");
	set_vida(1);
}

Canoa::Canoa(int eixo_x, int eixo_y)
{
	set_eixo_x(eixo_x);
	set_eixo_y(eixo_y);
}

Canoa::~Canoa(){}

int Canoa::danificar()
{

	cout << endl << endl << endl << " >> Você destruiu uma canoa!" << endl;
	set_vida(0);
	return 1;
}

int Canoa::rastrear(int eixo_x, int eixo_y)
{

	if(get_eixo_x() == eixo_x && get_eixo_y() == eixo_y){
		return danificar();
	}
	else
		return 0;
}
