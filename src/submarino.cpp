#include "submarino.hpp"
#include <iostream>


Submarino::Submarino()
{

	set_tamanho(2);
	set_eixo_x(0);
	set_eixo_y(0);
	set_orientacao("baixo");
	set_vida(2);
	set_sobrevida(2);
}

Submarino::Submarino(int eixo_x, int eixo_y, string orientacao)
{
	
	set_tamanho(2);
	set_eixo_x(eixo_x);
	set_eixo_y(eixo_y);
	set_orientacao(orientacao);
	set_vida(2);
	set_sobrevida(2);

	while(get_orientacao() == "cima" && eixo_y < 1)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}

	while(get_orientacao() == "baixo" && eixo_y > 11)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}

	while(get_orientacao() == "direita" && eixo_x > 11)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}

	while(get_orientacao() == "esquerda" && eixo_x < 1)
	{
		cout << endl << "A embarcação exedeu o mapa!" << endl << endl << "Escolha outra coordenada:" << endl;
	
		cin >> eixo_x >> eixo_y;
	
		set_eixo_x(eixo_x);
		set_eixo_y(eixo_y);
	}	
}

int Submarino::get_sobrevida()
{
	return sobrevida;
}

void Submarino::set_sobrevida(int sobrevida)
{
	this->sobrevida = sobrevida;
}

Submarino::~Submarino(){}

int Submarino::danificar(int eixo_x, int eixo_y)
{

	if(get_eixo_x() == eixo_x && get_eixo_y() == eixo_y)
	{

		if(get_vida() == 2)
		{
			set_vida(1);
			cout << endl << endl << endl  << " >> Você acertou parte de um submarino, atire novamente para destruí-la!" << endl;
			return 3;
		}

		else if(get_vida() == 1 && get_sobrevida() != 0)
		{
			set_vida(0);
			cout << endl << endl << endl << " >> Você destruiu parte de um submarino!" << endl;
			return 2;
		}

		else if(get_vida() == 1 && get_sobrevida() == 0)
		{
			set_vida(0);
			cout << endl << endl << endl << " >> Você destruiu um submarino!" << endl;
			return 1;
		}

		else
			return 0;
	}

	else if(get_eixo_x() != eixo_x || get_eixo_y() != eixo_y)
	{
		
		if(get_sobrevida() == 2)
		{
			set_sobrevida(1);
			cout << endl << endl << endl << " >> Você acertou parte de um submarino, atire novamente para destruí-la!" << endl;
			return 3;
		}

		else if(get_sobrevida() == 1 && get_vida() != 0)
		{
			set_sobrevida(0);
			cout << endl << endl << endl << " >> Você destruiu parte de um submarino!" << endl;
			return 2;
		}

		else if(get_sobrevida() == 1 && get_vida() == 0)
		{
			set_sobrevida(0);
			cout << endl << endl << endl <<" >> Você destruiu um submarino!" << endl;
			return 1;
		}
		
		else
			return 0;
		
	}
	
	else
		return 0;
}

int Submarino::rastrear(int eixo_x, int eixo_y)
{

	if(get_eixo_x() == eixo_x && get_eixo_y() == eixo_y)
		return danificar(eixo_x, eixo_y);

	else if(get_orientacao() == "direita" && get_eixo_y() == eixo_y && eixo_x == get_eixo_x() +1)
		return danificar(eixo_x, eixo_y);

	else if(get_orientacao() == "esquerda" && get_eixo_y() == eixo_y && eixo_x == get_eixo_x() -1)
		return danificar(eixo_x, eixo_y);

	else if(get_orientacao() == "cima" && get_eixo_x() == eixo_x && eixo_y == get_eixo_y() -1)
		return danificar(eixo_x, eixo_y);

	else if(get_orientacao() == "baixo" && get_eixo_x() == eixo_x && eixo_y == get_eixo_y() +1)
		return danificar(eixo_x, eixo_y);
	
	else
		return 0;	
}	
