#include "embarcacao.hpp"
#include <iostream>


Embarcacao::Embarcacao()
{

	tamanho = 1;
	eixo_x = 0;
	eixo_y = 0;
	vida = 1;
	orientacao = "nenhuma";

}

Embarcacao::~Embarcacao(){}

int Embarcacao::get_tamanho()
{
	return tamanho;
}

void Embarcacao::set_tamanho(int tamanho)
{
	this->tamanho = tamanho;
}

int Embarcacao::get_eixo_x()
{
	return eixo_x;
}

void Embarcacao::set_eixo_x(int eixo_x)
{
	this->eixo_x = eixo_x;
}

int Embarcacao::get_eixo_y()
{
	return eixo_y;
}

void Embarcacao::set_eixo_y(int eixo_y)
{
	this->eixo_y = eixo_y;
}

int Embarcacao::get_vida()
{
	return vida;
}

void Embarcacao::set_vida(int vida)
{
	this->vida = vida;
}

string Embarcacao::get_orientacao()
{
	return orientacao;
}

void Embarcacao::set_orientacao(string orientacao)
{
	this->orientacao = orientacao;
}

int Embarcacao::danificar()
{
	cout << "Você destruiu uma embarcação!" << endl;
	set_vida(0);
	return 1;	
}

int Embarcacao::rastrear(int eixo_x, int eixo_y)
{
	if(this->eixo_x == eixo_x && this->eixo_y == eixo_y)
		return danificar();
	else
		return false;
}

