#include <iostream>
#include <string>
#include <fstream>

#include "menu.hpp"
#include "mapa.hpp"
#include "jogo.hpp"


using namespace std;



int main(){
	
	Embarcacao *embarcacoes1[12];
	Embarcacao *embarcacoes2[12];

	string mapa_jogador1[13];
	string mapa_jogador2[13];

	Menu *menu = new Menu();

	Jogador *jogador1 = new Jogador();	
	Jogador *jogador2 = new Jogador();
	
	jogador1->set_mapa(13, mapa_jogador1);
	jogador2->set_mapa(13, mapa_jogador2);

	Jogo *jogo = new Jogo(jogador1, jogador2);

// Inicializa o menu

	menu->inicio();

// Coleta dos nomes dos jogadores

	jogo->jogadores();

// Geração das embarcações a partir do mapa

	jogo->cria_embarcacao(embarcacoes1, embarcacoes2);

	while(jogador1->get_num_embarcacoes() != 0 && jogador2->get_num_embarcacoes() != 0)
	{


		jogo->turno(jogador1, jogador2, mapa_jogador1); // turno(ataque, defesa, tabuleiro_ataque)
		

		jogo->turno(jogador2, jogador1, mapa_jogador2);
		

	}

// Declaração do vencedor e atualização do ranking

	jogo->atualiza_ranking(jogo->vencedor());

	delete jogo;	
	delete menu;
	delete jogador1;
	delete jogador2;

	return 0;
}	
