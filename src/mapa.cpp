#include "mapa.hpp"
#include <iostream>

using namespace std;


Mapa::Mapa()
{
	tamanho = 13;
	mapa = NULL;
}

Mapa::Mapa(int tamanho, string* mapa)
{
	this->tamanho = tamanho;
	this->mapa = mapa;

	for(int i=0;i<13;i++)
		for(int j=0;j<13;j++)
		{
			mapa[i][j] = ' ';
			mapa[i][j] = ' ';
		}
	
}

Mapa::~Mapa(){}


int Mapa::get_tamanho()
{
	return tamanho;
}

void Mapa::set_tamanho(int tamanho)
{
	this->tamanho = tamanho;
}

void Mapa::gera_mapa()
{
		cout << endl;
		cout << "   ";

		for(int i=0;i<get_tamanho();i++)
		{
			cout << "     ";
			cout << (char)('A'+ i);
		}
 	
		cout << endl;
  		cout << "      ";

   		for(int i=0; i<(6*get_tamanho())-1;i++)
			cout << "_";

		cout << endl;

		for(int i=0; i<get_tamanho();i++)
		{
			
			for(int j=0;j<(get_tamanho()+1);j++)
			{
				cout << "     ";
				cout << "|";
			}
			
			cout << endl;

			if(i<9)
				cout << i+1 << "  ";
			else
				cout << i+1 << " ";

			for(int j=0;j<get_tamanho();j++)
				cout << "  |  " << mapa[i][j];

			cout << "  |" << endl;
			
			cout << "     ";

			for(int j=0; j<get_tamanho();j++)
			{
				cout << "|";
				cout << "_____";
			}

			cout << "|" << endl;
		}

}
