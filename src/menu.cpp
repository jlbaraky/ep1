#include <iostream>
#include <fstream>
#include "menu.hpp"

using namespace std;


Menu::Menu(){}

Menu::~Menu(){}


void Menu::inicio()
{	
	int acao;

	cout << "\t______________________________________________________________________________________________________  " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                          ////////   //  ///     //  //     //  /////////                            | " << endl;
	cout << "\t|                          //     //  //  ////    //  //     //  //     //                            | " << endl;
	cout << "\t|                          //     //  //  // ///  //  //     //  //     //                            | " << endl;
	cout << "\t|                          ////////   //  //   // //  /////////  /////////                            | " << endl;
	cout << "\t|                          //     //  //  //    ////  //     //  //     //                            | " << endl;
	cout << "\t|                          //     //  //  //     ///  //     //  //     //                            | " << endl;
	cout << "\t|                          //     //  //  //      //  //     //  //     //                            | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                 DE                                                  | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                       ////////   /////////  ////////   ////////  //////////                         | " << endl;
	cout << "\t|                       //     //  //     //  //     //  //        //      //                         | " << endl;
	cout << "\t|                       //     //  //     //  //     //  //        //      //                         | " << endl;
	cout << "\t|                       ////////   /////////  ////////   //        //      //                         | " << endl;
	cout << "\t|                       //     //  //     //  //     //  //        //      //                         | " << endl;
	cout << "\t|                       //     //  //     //  //     //  //        //      //                         | " << endl;
	cout << "\t|                       ////////   //     //  //     //  ////////  //////////                         | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                        /||                                              ||\\                         | " << endl;
	cout << "\t|                       / ||                                              || \\                        | " << endl;
	cout << "\t|                      /  ||                       w                      ||  \\                       | " << endl;
	cout << "\t|                     /   ||   ________           _|_          _______    ||   \\                      | " << endl;
	cout << "\t|                    /____||  /     __()         (___)        ()__    \\   ||____\\                     | " << endl;
	cout << "\t|                _________||__|_____|                            |_____|__||__________                | " << endl;
	cout << "\t|               \\\\               //                               \\\\                //                | " << endl;
	cout << "\t|                \\\\_____________//                                 \\\\              //                 | " << endl;
	cout << "\t|                 \\\\___________//                                   \\\\____________//                  | " << endl;
	cout << "\t|             ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~              |" << endl;
	cout << "\t|             ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~              |" << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  [1] Jogar                                                                                          | " << endl;
	cout << "\t|  [2] Instruções                                                                                     | " << endl;
	cout << "\t|  [3] Ranking                                                                                        | " << endl;
	cout << "\t|  [4] Sair                                                                                           | " << endl;
	cout << "\t|_____________________________________________________________________________________________________| " << endl;
	
	
	cin >> acao;
	cout << acao << endl;
	cin.ignore();

	switch (acao)
	{
		case 1:
			limpa_tela();
			return;
			break;
		case 2:
			limpa_tela();
			instrucoes();
			break;
		case 3:
			limpa_tela();
			ranking();
			break;
		case 4:
			limpa_tela();
			exit(EXIT_SUCCESS);
			break;
		default:
			limpa_tela();
			inicio();
	}

}	

void Menu::jogar()
{

}

void Menu::instrucoes()
{


	cout << "\t______________________________________________________________________________________________________  " << endl;	
	cout << "\t|                                                                                                     | " << endl;
   	cout << "\t|  INSTRUÇÕES                                                                                         | " << endl;
	cout << "\t|_____________________________________________________________________________________________________| " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|    SOBRE:                                                                                           |       " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|           Batalha Naval é um jogo de 2 jogadores, no qual ambos jogadores possuem embarcações       | " << endl;
	cout << "\t|    espalhadas pelo mapa. Vence aquele que destuir todos os navios do oponente.                      | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|    REGRAS:                                                                                          |      " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|           Cada jogador, em seu turno, decide uma coordenada para efetuar um ataque (Ex: 4 G).       | " << endl;
	cout << "\t|    É verificado se há alguma embarcação no local, se sim o navio é danificado e/ou destruído,       | " << endl;
	cout << "\t|    dependendo do seu tipo.                                                                          | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|    SIMBOLOS:                                                                                        | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|      [ x ] - Unidade destruída                                                                      | " << endl;
	cout << "\t|      [ o ] - Unidade acertada porém não destruída                                                   | " << endl;
	cout << "\t|      [ - ] - Míssil caiu na água                                                                    | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|   EMBARCAÇÕES                                                                                       | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|        _______________________         _______________________          _______________________     | " << endl;
	cout << "\t|       |                       |       |                       |        |                       |    | " << endl;
	cout << "\t|       |                       |       |                       |        |                       |    | " << endl;
	cout << "\t|       |                       |       |                       |        |                       |    | " << endl;
	cout << "\t|       |                       |       |                       |        |                       |    | " << endl;
	cout << "\t|       |                       |       |         ____          |        |               /|      |    | " << endl;
	cout << "\t|       |                       |       |         | _()         |        |  _______   __/ |___/| |    | " << endl;
	cout << "\t|       |    ________//____     |       |~~~~~~~~~||~~~~~~~~~~~~|        | /      |  (_________| |    | " << endl;
	cout << "\t|       |    \\      //    /     |       |   ______||________    |        | |0  o o|______|______ |    | " << endl;
	cout << "\t|       |     \\____//____/      |       |  /                \\   |        | \\__________________/  |    | " << endl;
	cout << "\t|       |~~~~~~~~~//~~~~~~~~~~~~|       | (  o   o   o   o   )  |        |~~~~~~~~~~~~~~~~~~~~~~~|    | " << endl;
	cout << "\t|       |                       |       |  \\________________/   |        |                       |    | " << endl;
	cout << "\t|       |_______________________|       |_______________________|        |_______________________|    | " << endl;
	cout << "\t|       |                       |       |                       |        |                       |    | " << endl;
	cout << "\t|       |- Tipo: Canoa          |       |- Tipo: Submarino      |        |- Tipo: Porta aviões   |    | " << endl;
	cout << "\t|       |- Tamanho: 1           |       |- Tamanho: 2           |        |- Tamanho: 4           |    | " << endl;
	cout << "\t|       |- Habilidade: Nenhuma  |       |- Habilidade: Defende  | 	 |- Habilidade: É capaz  |    |" << endl;
	cout << "\t|       |                       |       | 1 míssil em cada casa |        |  de deviar o míssil   |    | " << endl;
	cout << "\t|       |_______________________|       |_______________________|        |_______________________|    | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t| [Aperte ENTER para voltar]                                                                          | " << endl;
	cout << "\t|_____________________________________________________________________________________________________| " << endl;

	
	getchar();
	limpa_tela();
	inicio();
}

void Menu::ranking()
{

	fstream rank;

	rank.open("doc/ranking.txt");

	string nome[10];
	int pontos[10];
	int espaco[10];
	int tamanho_pontos;
	int aux;
	for(int i=0;i<10;i++)
	{
		rank >> nome[i] >> pontos[i];
		
		tamanho_pontos = 1;
		aux = pontos[i];
		
		while(aux /= 10)
			tamanho_pontos++;

		espaco[i] = nome[i].length() + tamanho_pontos;
	}

	rank.close();

	cout << "\t______________________________________________________________________________________________________  " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  RANKING                                                                                            | " << endl;
	cout << "\t|_____________________________________________________________________________________________________| " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  1º - " << nome[0] <<  " " << pontos[0] << " vitórias"; espacamento(73-espaco[0]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  2º - " << nome[1] <<  " " << pontos[1] << " vitórias"; espacamento(73-espaco[1]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  3º - " << nome[2] <<  " " << pontos[2] << " vitórias"; espacamento(73-espaco[2]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  4º - " << nome[3] <<  " " << pontos[3] << " vitórias"; espacamento(73-espaco[3]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  5º - " << nome[4] <<  " " << pontos[4] << " vitórias"; espacamento(73-espaco[4]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  6º - " << nome[5] <<  " " << pontos[5] << " vitórias"; espacamento(73-espaco[5]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  7º - " << nome[6] <<  " " << pontos[6] << " vitórias"; espacamento(73-espaco[6]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  8º - " << nome[7] <<  " " << pontos[7] << " vitórias"; espacamento(73-espaco[7]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  9º - " << nome[8] <<  " " << pontos[8] << " vitórias"; espacamento(73-espaco[8]); cout << "           | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  10º - " << nome[9] <<  " " << pontos[9] << " vitórias"; espacamento(73-espaco[9]); cout << "          | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|                                                                                                     | " << endl;
	cout << "\t|  [Aperte ENTER para voltar ou R para reiniciar o ranking]                                           | " << endl;
	cout << "\t|_____________________________________________________________________________________________________| " << endl;

	char acao = getchar();
	limpa_tela();

	acao = toupper(acao);

	if(acao == 'R')
	{
		cout << endl << endl << endl << endl << endl << endl << endl << endl;
		cout << "\t\t\t _______________________________________________" << endl;
		cout << "\t\t\t |                                              |" << endl;
		cout << "\t\t\t |                                              |" << endl;
		cout << "\t\t\t |     Você tem certeza que deseja reiniciar    |" << endl;
		cout << "\t\t\t |                                              |" << endl;
		cout << "\t\t\t |      o Ranking?                              |" << endl;
		cout << "\t\t\t |                                              |" << endl;
		cout << "\t\t\t |                           [S ou N]           |" << endl;
		cout << "\t\t\t |______________________________________________|" << endl;
		
		char opcao;
		cin >> opcao;
		getchar();
		opcao = toupper(opcao);

		if(opcao == 'S')
		{
			limpa_tela();
			limpa_ranking();
		}

		else
		{
			limpa_tela();

	}
	ranking();
	}

	inicio();
}

void Menu::limpa_ranking()
{
	ofstream rank;
	rank.open("doc/ranking.txt");

	string nome = "-----";
	int pontos = 0;
	
	for(int i=0;i<10;i++)
		rank << nome << " " << pontos << endl;
}

void Menu::limpa_tela()
{
	system("clear");
}

void Menu::espacamento(int num)
{
	for(int i=0;i<num;i++)
		cout << " ";
}
