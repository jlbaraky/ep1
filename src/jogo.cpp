#include "jogo.hpp"
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>

using namespace std;

Jogo::Jogo()
{

	jogador1 = NULL;
	jogador2 = NULL;
}

Jogo::Jogo(Jogador* jogador1, Jogador* jogador2)
{

	this->jogador1 = jogador1;
	this->jogador2 = jogador2;
}

Jogo::~Jogo(){}

void Jogo::jogadores()
{

	string nome1;
	string nome2;
	

	cout << endl << "[Jogador 1] Username:" << endl;
	cin >> nome1;


	cout << endl << "[Jogador 2] Username:" << endl;
	cin >> nome2;

	while(nome2 == nome1)
	{
		cout << endl << "Username repetido, tente novamente:" << endl;
		cin >> nome2;
	}

	jogador1->set_nome(nome1);
	jogador2->set_nome(nome2);
}

Embarcacao* Jogo::retorna_embarcacao(int eixo_x, int eixo_y, string tipo, string orientacao)
{


	if(tipo == "canoa")
	{
		Canoa *canoa = new Canoa(eixo_x, eixo_y);
		return canoa;
	}

	else if(tipo == "submarino")
	{
		Submarino *submarino = new Submarino(eixo_x, eixo_y, orientacao);
		return submarino;
	}

	else
	{
		Porta_avioes *porta_avioes = new Porta_avioes(eixo_x, eixo_y, orientacao);
		return porta_avioes;
	}	
}

void Jogo::cria_embarcacao(Embarcacao** embarcacoes1, Embarcacao**embarcacoes2)
{

	ifstream mapa;
	int eixo_x, eixo_y;
	string tipo, orientacao;

// Selecionando mapas aleatoriamente
	
	srand (time(NULL));

	int r = rand() % 3 + 1;


	stringstream ss;

	ss << "doc/map_" << r << ".txt";


	string filename = ss.str();

	mapa.open(filename);



	char aux;
	string comment;

	int j = 0;
	int k = 0;

// Coleta dos dados do mapa
	while(!mapa.eof())
	{

		aux = mapa.peek();

		if(aux == ' ' || aux == '#' || aux == '\n')
		{
			getline(mapa, comment);
			if(comment == "# player_2")
				j = -1;
			continue;
		}

		mapa >> eixo_y >> eixo_x >> tipo >> orientacao;
		
		if(mapa.eof())
			break;

		
		if(j>=0)
		{
			embarcacoes1[j] = retorna_embarcacao(eixo_x, eixo_y, tipo, orientacao);
			j++;
		}
		else
		{
			embarcacoes2[k] = retorna_embarcacao(eixo_x, eixo_y, tipo, orientacao);
			k++;
		}
	}

	jogador1->set_embarcacoes(embarcacoes1);
	jogador2->set_embarcacoes(embarcacoes2);

	mapa.close();
}

void Jogo::le_coordenadas(int *eixo_x, int *eixo_y)
{
	int coor_x;
	int coor_y;
	char letra;

	cin >> coor_y >> letra;
	cin.ignore();
	letra = toupper(letra);
	
	while(isdigit(letra) || coor_y < 0 || coor_y > 13 || letra > char('A'+12))
	{
		cout << endl << "Porfavor, digite as coordenadas corretamente. (Ex: 6 H)" << endl;
		cin >> coor_y >> letra;
		cin.ignore();
		letra = toupper(letra);
	}


	switch (letra){
		case 'A':
			coor_x = 1;
			break;
		case 'B':
			coor_x = 2;
			break;
		case  'C':
			coor_x = 3;
			break;
		case  'D':
			coor_x = 4;
			break;
		case  'E':
			coor_x = 5;
			break;
		case  'F':
			coor_x = 6;
			break;
		case  'G':
			coor_x = 7;
			break;
		case  'H':
			coor_x = 8;
			break;
		case  'I':
			coor_x = 9;
			break;
		case  'J':
			coor_x = 10;
			break;
		case  'K':
			coor_x = 11;
			break;
		case  'L':
			coor_x = 12;
			break;
		case  'M':
			coor_x = 13;
			break;
		case  'N':
			coor_x = 14;
			break;
		case  'o':
			coor_x = 15;
			break;
		case  'P':
			coor_x = 16;
			break;
	}

	*eixo_x = coor_x - 1;
	*eixo_y = coor_y - 1;
}


void Jogo::turno(Jogador *jogador1, Jogador *jogador2, string *mapa_jogador)
{

	
	this->limpa_tela();

	jogador1->tabuleiro();

	int eixo_x, eixo_y;

	cout << endl << jogador1->get_nome() <<", é a sua vez!" << endl;

	cout << endl <<  "Digite as coordenadas que deseja efetuar o ataque:" << endl;
	
	le_coordenadas(&eixo_x, &eixo_y);

	int acao;

	while(mapa_jogador[eixo_y][eixo_x] == 'x' || mapa_jogador[eixo_y][eixo_x] == '-')
	{
		cout << endl << "Você já verificou esse local, digite outra coordenada:" << endl;
		le_coordenadas(&eixo_x, &eixo_y);
	}

	this->limpa_tela();

	acao = jogador1->atacar(jogador2, eixo_x, eixo_y);

	if(acao == 1)
			jogador2->subtrair_embarcacoes();
		

	jogador1->set_pontuacao(12 - jogador2->get_num_embarcacoes());

	switch (acao)
	{
		
		case 0:
			cout << endl << endl << endl << " >> O míssil caiu na água!" << endl;
			mapa_jogador[eixo_y][eixo_x] = '-';
			jogador1->tabuleiro();

			cout << endl << "Você está com " << jogador1->get_pontuacao() << " pontos!" << endl;
			cout << endl << "[Aperte ENTER para passar a vez]" << endl;
			getchar();
			break;

		case 1:
			mapa_jogador[eixo_y][eixo_x] = 'x';
			jogador1->tabuleiro();
			cout << endl << "Você está com " << jogador1->get_pontuacao() << " pontos!" << endl;
			cout << endl << "[Aperte ENTER para passar a vez]" << endl;
			getchar();
			break;

		case 2:
			mapa_jogador[eixo_y][eixo_x] = 'x';
			jogador1->tabuleiro();
			cout << endl << "Você está com " << jogador1->get_pontuacao() << " pontos!" << endl;
			cout << endl << "[Aperte ENTER para passar a vez]" << endl;
			getchar();
			break;

		case 3:
			mapa_jogador[eixo_y][eixo_x] = 'o';
			jogador1->tabuleiro();
			cout << endl << "Você está com " << jogador1->get_pontuacao() << " pontos!" << endl;
			cout << endl << "[Aperte ENTER para passar a vez]" << endl;
			getchar();
			break;
	}

}

Jogador* Jogo::vencedor()
{
	
	int espaco_nome1 = jogador1->get_nome().length();
	int espaco_nome2 = jogador2->get_nome().length();



	if(jogador2->get_num_embarcacoes() == 0)
	{
		cout << endl << endl << endl << endl << endl << endl << endl << endl << endl;
		cout << "\t\t\t\t  __________________________________________________ " << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |               Parabéns " << jogador1->get_nome() << "!"; for(int i=0;i<(20-espaco_nome1);i++){cout << " ";} cout << "     |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |              Você ganhou a partida!              |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |__________________________________________________|" << endl;	
		cout << endl << endl << endl << endl << endl << endl << endl << endl << endl;
		return jogador1;
	}
	else
	{
		cout << endl << endl << endl << endl << endl << endl << endl << endl << endl;
		cout << "\t\t\t\t  __________________________________________________ " << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |               Parabéns " << jogador2->get_nome() << "!"; for(int i=0;i<(20-espaco_nome2);i++){cout << " ";} cout << "     |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |              Você ganhou a partida!              |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |                                                  |" << endl;
		cout << "\t\t\t\t |__________________________________________________|" << endl;
		cout << endl << endl << endl << endl << endl << endl << endl << endl << endl;
		return jogador2;
	}
}

void Jogo::atualiza_ranking(Jogador* jogador)
{
	ifstream le_rank;
	ofstream escreve_rank;
	string nome[10];
	int pontos[10];
	int ordena[10] = {0,1,2,3,4,5,6,7,8,9};
	int aux = 0;
	int index;
	int x;

	le_rank.open("doc/ranking.txt");

// Leitura do ranking atual
	
	for(int i=0; i<10;i++)
	{
		le_rank >> nome[i] >> pontos[i];

		if(nome[i] == jogador->get_nome())
		{
			pontos[i]++;
			aux = 1;
		}

		if(nome[i] == "-----" && aux == 0)
		{
			nome[i] = jogador->get_nome();
			pontos[i]++;
			aux = 1;
		}
	}
	
// Ordena os vetores e recolhe os índices

	for(int i=0;i<10;i++)
	{
		aux = pontos[ordena[i]];
		index = ordena[i];

		for(int j=i;j<10;j++)
		{
			if(pontos[j] > aux)
			{
				aux = pontos[j];
				index = j;
			}
		}

		pontos[index] = pontos[ordena[i]];
		pontos[ordena[i]] = aux;

		ordena[index] = i;
		ordena[i] = index;
	}


	le_rank.close();

	escreve_rank.open("doc/ranking.txt");
	
// Escreve o ranking atualizado

	for(int i=0; i<10;i++)
	{
		x = ordena[i];
		escreve_rank << nome[x] << " " << pontos[i] << endl;
	}

	escreve_rank.close();
}

void Jogo::limpa_tela()
{
	system("clear");
}

